# kashevdalmia.com

    Kashev Dalmia - kashev.dalmia@gmail.com
    kashevdalmia.com

This repository contains the content of the web site http://kashevdalmia.com.

## Instructions
Building this repo requires `node`, `npm`, and `ruby`.

To install JS dependencies, run `npm install`, `npm install -g grunt-cli`.

To develop, run `grunt server`.

To compile Sass files, `gem install sass`.

## Thanks
This project is possible thanks to the work on the following projects:

- [Node](http://nodejs.org/)
- [Grunt](http://gruntjs.com/)
- [Sass](http://sass-lang.com/)
- [Pure CSS](http://purecss.io/)
- [hint.css](http://kushagragour.in/lab/hint/)
- [Simple Icons](http://simpleicons.org/)
- Thanks to [Rob Jepson](http://robjepson.wordpress.com/) for this [nifty resume icon](http://robjepson.wordpress.com/2013/04/16/resume-icon/) which I swiped.
